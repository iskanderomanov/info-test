fpm:
	docker compose exec php bash
artisan:
	docker exec php-container php artisan $(a)
composer:
	docker exec php-container composer $(a)
composer-i:
	docker exec php-container composer install
npm-install:
	docker compose exec node npm install $(a)
npm-uninstall:
	docker compose exec node npm uninstall $(a)
npm-build:
	docker compose exec node npm run build $(a)
npm-dev:
	docker compose exec node npm run dev $(a)
tinker:
	docker compose exec -u 0 php php artisan tinker