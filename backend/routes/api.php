<?php

use App\Enums\RouteNames;
use App\Http\Controllers\Backend\DepartmentController;
use App\Http\Controllers\Backend\EmployeeController;
use App\Http\Controllers\Backend\OrganizationController;
use App\Http\Controllers\Backend\AuthController;
use App\Http\Controllers\Backend\PositionController;
use Illuminate\Support\Facades\Route;


Route::prefix('v1/')->group(function () {
    Route::post(RouteNames::LoginUserRoute->toRouteUrl(), [AuthController::class, 'login'])->name(RouteNames::LoginUserRoute->value);

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get(RouteNames::LogoutUserRoute->toRouteUrl(), [AuthController::class, 'logout'])->name(RouteNames::LogoutUserRoute->value);
        Route::get(RouteNames::GetUserRoute->toRouteUrl(), [AuthController::class, 'getUser'])->name(RouteNames::GetUserRoute->value);

        Route::get(RouteNames::IndexOrganizationRoute->toRouteUrl(), [OrganizationController::class, 'index'])->name(RouteNames::IndexOrganizationRoute->value);
        Route::post(RouteNames::StoreOrganizationRoute->toRouteUrl(), [OrganizationController::class, 'store'])->name(RouteNames::StoreOrganizationRoute->value);
        Route::get(RouteNames::ShowOrganizationRoute->toRouteUrl(), [OrganizationController::class, 'show'])->name(RouteNames::ShowOrganizationRoute->value);
        Route::put(RouteNames::UpdateOrganizationRoute->toRouteUrl(), [OrganizationController::class, 'update'])->name(RouteNames::UpdateOrganizationRoute->value);
        Route::delete(RouteNames::DestroyOrganizationRoute->toRouteUrl(), [OrganizationController::class, 'destroy'])->name(RouteNames::DestroyOrganizationRoute->value);

        Route::get(RouteNames::IndexDepartmentRoute->toRouteUrl(), [DepartmentController::class, 'index'])->name(RouteNames::IndexDepartmentRoute->value);
        Route::post(RouteNames::StoreDepartmentRoute->toRouteUrl(), [DepartmentController::class, 'store'])->name(RouteNames::StoreDepartmentRoute->value);
        Route::get(RouteNames::ShowDepartmentRoute->toRouteUrl(), [DepartmentController::class, 'show'])->name(RouteNames::ShowDepartmentRoute->value);
        Route::put(RouteNames::UpdateDepartmentRoute->toRouteUrl(), [DepartmentController::class, 'update'])->name(RouteNames::UpdateDepartmentRoute->value);
        Route::delete(RouteNames::DestroyDepartmentRoute->toRouteUrl(), [DepartmentController::class, 'destroy'])->name(RouteNames::DestroyDepartmentRoute->value);

        Route::get(RouteNames::IndexPositionRoute->toRouteUrl(), [PositionController::class, 'index'])->name(RouteNames::IndexPositionRoute->value);
        Route::post(RouteNames::StorePositionRoute->toRouteUrl(), [PositionController::class, 'store'])->name(RouteNames::StorePositionRoute->value);
        Route::get(RouteNames::ShowPositionRoute->toRouteUrl(), [PositionController::class, 'show'])->name(RouteNames::ShowPositionRoute->value);
        Route::put(RouteNames::UpdatePositionRoute->toRouteUrl(), [PositionController::class, 'update'])->name(RouteNames::UpdatePositionRoute->value);
        Route::delete(RouteNames::DestroyPositionRoute->toRouteUrl(), [PositionController::class, 'destroy'])->name(RouteNames::DestroyPositionRoute->value);

        Route::get(RouteNames::IndexEmployeeRoute->toRouteUrl(), [EmployeeController::class, 'index'])->name(RouteNames::IndexEmployeeRoute->value);
        Route::post(RouteNames::StoreEmployeeRoute->toRouteUrl(), [EmployeeController::class, 'store'])->name(RouteNames::StoreEmployeeRoute->value);
        Route::get(RouteNames::ShowEmployeeRoute->toRouteUrl(), [EmployeeController::class, 'show'])->name(RouteNames::ShowEmployeeRoute->value);
        Route::put(RouteNames::UpdateEmployeeRoute->toRouteUrl(), [EmployeeController::class, 'update'])->name(RouteNames::UpdateEmployeeRoute->value);
        Route::delete(RouteNames::DestroyEmployeeRoute->toRouteUrl(), [EmployeeController::class, 'destroy'])->name(RouteNames::DestroyEmployeeRoute->value);
    });
});
