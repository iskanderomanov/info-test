<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RouteNames;
use App\Utils\Columns\DepartmentColumns;
use App\Utils\Columns\FilterColumns;
use App\Utils\Columns\OrganizationColumns;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return match ($this->route()->getName()) {
            RouteNames::IndexDepartmentRoute->value => [
                FilterColumns::TAKE => 'nullable|integer|min:0|max:500',
                FilterColumns::SKIP => 'nullable|integer',
                FilterColumns::FROM_CREATED_AT => 'nullable|date',
                FilterColumns::TO_CREATED_AT => 'nullable|date|after_or_equal:from_created_at',
                FilterColumns::ORDER_BY => 'nullable|string',
                FilterColumns::DIRECTION => 'nullable|string|in:asc,desc',

                DepartmentColumns::ORGANIZATION_ID => 'integer|exists:' . OrganizationColumns::TABLE_NAME . ',id|nullable',
            ],
            RouteNames::StoreDepartmentRoute->value => [
                DepartmentColumns::NAME => 'required|string|max:255',
                DepartmentColumns::DESCRIPTION => 'nullable|string|max:4000',
                DepartmentColumns::ORGANIZATION_ID => 'required|integer|exists:' . OrganizationColumns::TABLE_NAME . ',id',
            ],
            RouteNames::UpdateDepartmentRoute->value => [
                DepartmentColumns::NAME => 'string|max:255',
                DepartmentColumns::DESCRIPTION => 'nullable|string|max:4000',
                DepartmentColumns::ORGANIZATION_ID => 'integer|exists:' . OrganizationColumns::TABLE_NAME . ',id',
            ],
            default => []
        };
    }
}
