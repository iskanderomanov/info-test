<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RouteNames;
use App\Utils\Columns\UserColumns;
use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
         return match($this->route()->getName()){
            RouteNames::LoginUserRoute->value => [
                UserColumns::EMAIL => 'required|string|email|exists:' . UserColumns::TABLE_NAME . ',email',
                UserColumns::PASSWORD => 'required|string|min:8',
            ],
            default => []
        };
    }

    protected function prepareForValidation()
    {

    }
}
