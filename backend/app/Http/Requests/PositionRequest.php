<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RouteNames;
use App\Utils\Columns\PositionColumns;
use App\Utils\Columns\FilterColumns;
use App\Utils\Columns\DepartmentColumns;
use Illuminate\Foundation\Http\FormRequest;

class PositionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return match ($this->route()->getName()) {
            RouteNames::IndexPositionRoute->value => [
                FilterColumns::TAKE => 'nullable|integer|min:0|max:500',
                FilterColumns::SKIP => 'nullable|integer',
                FilterColumns::FROM_CREATED_AT => 'nullable|date',
                FilterColumns::TO_CREATED_AT => 'nullable|date|after_or_equal:from_created_at',
                FilterColumns::ORDER_BY => 'nullable|string',
                FilterColumns::DIRECTION => 'nullable|string|in:asc,desc',

                PositionColumns::DEPARTMENT_ID => 'integer|exists:' . DepartmentColumns::TABLE_NAME . ',id|nullable',
            ],
            RouteNames::StorePositionRoute->value => [
                PositionColumns::NAME => 'required|string|max:255',
                PositionColumns::DESCRIPTION => 'nullable|string|max:4000',
                PositionColumns::DEPARTMENT_ID => 'required|integer|exists:' . DepartmentColumns::TABLE_NAME . ',id',
            ],
            RouteNames::UpdatePositionRoute->value => [
                PositionColumns::NAME => 'string|max:255',
                PositionColumns::DESCRIPTION => 'nullable|string|max:4000',
                PositionColumns::DEPARTMENT_ID => 'integer|exists:' . DepartmentColumns::TABLE_NAME . ',id',
            ],
            default => []
        };
    }
}
