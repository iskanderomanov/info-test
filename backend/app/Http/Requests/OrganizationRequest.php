<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RouteNames;
use App\Utils\Columns\FilterColumns;
use App\Utils\Columns\OrganizationColumns;
use Illuminate\Foundation\Http\FormRequest;

class OrganizationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
         return match($this->route()->getName()){
            RouteNames::IndexOrganizationRoute->value => [
                FilterColumns::TAKE => 'nullable|integer|min:0|max:500',
                FilterColumns::SKIP => 'nullable|integer',
                FilterColumns::FROM_CREATED_AT => 'nullable|date',
                FilterColumns::TO_CREATED_AT => 'nullable|date|after_or_equal:from_created_at',
                FilterColumns::ORDER_BY => 'nullable|string',
                FilterColumns::DIRECTION => 'nullable|string|in:asc,desc',
            ],
            RouteNames::StoreOrganizationRoute->value => [
                OrganizationColumns::NAME => 'required|string|max:255',
                OrganizationColumns::DESCRIPTION => 'nullable|string|max:4000',
            ],
            RouteNames::UpdateOrganizationRoute->value => [
                OrganizationColumns::NAME => 'string|max:255',
                OrganizationColumns::DESCRIPTION => 'nullable|string|max:4000',
            ],
            default => []
        };
    }
}
