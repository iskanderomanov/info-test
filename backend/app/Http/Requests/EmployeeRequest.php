<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\RouteNames;
use App\Utils\Columns\EmployeeColumns;
use App\Utils\Columns\FilterColumns;
use App\Utils\Columns\DepartmentColumns;
use App\Utils\Columns\PositionColumns;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return match ($this->route()->getName()) {
            RouteNames::IndexEmployeeRoute->value => [
                FilterColumns::TAKE => 'nullable|integer|min:0|max:500',
                FilterColumns::SKIP => 'nullable|integer',
                FilterColumns::FROM_CREATED_AT => 'nullable|date',
                FilterColumns::TO_CREATED_AT => 'nullable|date|after_or_equal:from_created_at',
                FilterColumns::ORDER_BY => 'nullable|string',
                FilterColumns::DIRECTION => 'nullable|string|in:asc,desc',

                EmployeeColumns::POSITION_ID => 'integer|exists:' . PositionColumns::TABLE_NAME . ',id|nullable',
            ],
            RouteNames::StorePositionRoute->value => [
                EmployeeColumns::FIRST_NAME => 'required|string|max:255',
                EmployeeColumns::MIDDLE_NAME => 'required|string|max:255',
                EmployeeColumns::LAST_NAME => 'required|string|max:255',
                EmployeeColumns::BIRTH_DATE => 'required|date|after:1930-01-01',
                EmployeeColumns::IS_MAN => 'required|bool',
                EmployeeColumns::POSITION_ID => 'required|integer|exists:' . PositionColumns::TABLE_NAME . ',id',
            ],
            RouteNames::UpdateEmployeeRoute->value => [
                EmployeeColumns::FIRST_NAME => 'string|max:255',
                EmployeeColumns::MIDDLE_NAME => 'string|max:255',
                EmployeeColumns::LAST_NAME => 'string|max:255',
                EmployeeColumns::BIRTH_DATE => 'date|after:1930-01-01',
                EmployeeColumns::IS_MAN => 'int|bool',
                EmployeeColumns::POSITION_ID => 'integer|exists:' . PositionColumns::TABLE_NAME . ',id',
            ],
            default => []
        };
    }
}
