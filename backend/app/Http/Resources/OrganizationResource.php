<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Utils\Columns\OrganizationColumns;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrganizationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            OrganizationColumns::ID => $this->id,
            OrganizationColumns::NAME => $this->name,
            OrganizationColumns::DESCRIPTION => $this->description,
            OrganizationColumns::CREATED_AT => date_format($this->created_at, 'Y-m-d H:i:s'),
        ];
    }
}
