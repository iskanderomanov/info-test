<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Utils\Columns\UserColumns;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            UserColumns::ID => $this->id,
            UserColumns::NAME => $this->name,
            UserColumns::EMAIL => $this->email,
            UserColumns::CREATED_AT => $this->created_at,
            UserColumns::UPDATED_AT => $this->updated_at,
        ];
    }
}
