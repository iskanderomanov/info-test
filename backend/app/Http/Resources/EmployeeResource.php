<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Utils\Columns\EmployeeColumns;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            EmployeeColumns::ID => $this->{EmployeeColumns::ID},
            EmployeeColumns::FIRST_NAME => $this->{EmployeeColumns::FIRST_NAME},
            EmployeeColumns::MIDDLE_NAME => $this->{EmployeeColumns::MIDDLE_NAME},
            EmployeeColumns::LAST_NAME => $this->{EmployeeColumns::LAST_NAME},
            EmployeeColumns::BIRTH_DATE => $this->{EmployeeColumns::BIRTH_DATE},
            EmployeeColumns::IS_MAN => $this->{EmployeeColumns::IS_MAN},
            EmployeeColumns::POSITION_ID => $this->{EmployeeColumns::POSITION_ID},
            EmployeeColumns::CREATED_AT => date_format($this->created_at, 'Y-m-d H:i:s'),
        ];
    }
}
