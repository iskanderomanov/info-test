<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Utils\Columns\PositionColumns;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PositionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            PositionColumns::ID => $this->id,
            PositionColumns::NAME => $this->name,
            PositionColumns::DESCRIPTION => $this->description,
            PositionColumns::DEPARTMENT_ID => $this->department_id,
            PositionColumns::CREATED_AT => date_format($this->created_at, 'Y-m-d H:i:s'),
        ];
    }
}
