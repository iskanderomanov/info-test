<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Utils\Columns\DepartmentColumns;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            DepartmentColumns::ID => $this->id,
            DepartmentColumns::NAME => $this->name,
            DepartmentColumns::DESCRIPTION => $this->description,
            DepartmentColumns::ORGANIZATION_ID => $this->organization_id,
            DepartmentColumns::CREATED_AT => date_format($this->created_at, 'Y-m-d H:i:s'),
        ];
    }
}
