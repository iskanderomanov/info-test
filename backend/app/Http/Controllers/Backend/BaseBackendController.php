<?php
declare(strict_types=1);


namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * @OA\Info(
 *      title="Info test",
 *      version="1.0.0",
 *      @OA\Contact(
 *          email="iskanderomanov@gmail.com"
 *      ),
 * )
 * @OA\SecurityScheme(
 *      type="http",
 *      scheme="bearer",
 *      securityScheme="bearerAuth"
 *  )
 * @OA\Schema(
 *      schema="ValidationError",
 *      @OA\Property(property="message", type="string"),
 *      @OA\Property(property="errors", type="object")
 *  )
 */
class BaseBackendController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
}
