<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\DTO\PositionDto;
use App\DTO\PositionFilterDto;
use App\Http\Requests\PositionRequest;
use App\Http\Resources\PositionResource;
use App\Services\Backend\PositionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PositionController extends BaseBackendController
{
    public function __construct(
        private readonly PositionService   $service,
        private readonly PositionDto       $dto,
        private readonly PositionFilterDto $filterDto
    )
    {
    }

    /**
     * @OA\Get(
     *     path="/api/v1/positions",
     *     summary="List positions",
     *     description="Retrieve a list of positions",
     *     tags={"Position"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="take",
     *         in="query",
     *         description="Number of records to take",
     *         required=false,
     *         @OA\Schema(type="integer", format="int32", minimum=0, maximum=500)
     *     ),
     *     @OA\Parameter(
     *         name="skip",
     *         in="query",
     *         description="Number of records to skip",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="from_created_at",
     *         in="query",
     *         description="Filter positions created after this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="to_created_at",
     *         in="query",
     *         description="Filter positions created before this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="order_by",
     *         in="query",
     *         description="Order by a specific field",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="direction",
     *         in="query",
     *         description="Sort direction (asc or desc)",
     *         required=false,
     *         @OA\Schema(type="string", enum={"asc", "desc"})
     *     ),
     *     @OA\Parameter(
     *         name="department_id",
     *         in="query",
     *         description="Filter positions by department ID",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/PositionResourceCollection"),
     *
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     * @OA\Schema(
     *       schema="PositionResourceCollection",
     *                   type="array",
     *               @OA\Items(
     *           @OA\Property(property="id", type="integer"),
     *           @OA\Property(property="name", type="string"),
     *           @OA\Property(property="description", type="string"),
     *           @OA\Property(property="department_id", type="integer"),
     *           @OA\Property(property="created_at", type="string", format="date-time"),
     *               )
     *   )
     * @OA\Schema(
     *       schema="PositionResource",
     *           @OA\Property(property="id", type="integer"),
     *           @OA\Property(property="name", type="string"),
     *           @OA\Property(property="description", type="string"),
     *           @OA\Property(property="department_id", type="integer"),
     *           @OA\Property(property="created_at", type="string", format="date-time"),
     *   )
     */
    public function index(PositionRequest $request): AnonymousResourceCollection
    {
        $positions = $this->service->index(
            $this->filterDto::create($request->all())
        );
        return PositionResource::collection($positions);
    }


    /**
     * @OA\Post(
     *     path="/api/v1/positions",
     *     summary="Create a new position",
     *     description="Create a new position record",
     *     tags={"Position"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Position data",
     *         @OA\JsonContent(
     *             required={"name", "department_id"},
     *             @OA\Property(property="name", type="string", example="Manager"),
     *             @OA\Property(property="description", type="string", example="Description of the position"),
     *             @OA\Property(property="department_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Position created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/PositionResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function store(PositionRequest $request): PositionResource
    {
        $position = $this->service->store(
            $this->dto::create($request->all())
        );

        return new PositionResource($position);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/positions/{positionId}",
     *     summary="Get a position by ID",
     *     description="Retrieve a position record by its ID",
     *     tags={"Position"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="positionId",
     *         in="path",
     *         description="ID of the position to retrieve",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Position retrieved successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/PositionResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Position not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Position not found")
     *         )
     *     )
     * )
     */
    public function show(int $positionId): PositionResource
    {
        $position = $this->service->show($positionId);
        return new PositionResource($position);
    }

    /**
     * @OA\Put(
     *     path="/api/v1/positions/{positionId}",
     *     summary="Update an existing position",
     *     description="Update an existing position record",
     *     tags={"Position"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="positionId",
     *         in="path",
     *         description="ID of the position to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated position data",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Manager"),
     *             @OA\Property(property="description", type="string", example="Description of the position"),
     *             @OA\Property(property="department_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Position updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/PositionResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Position not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Position not found")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function update(PositionRequest $request, int $positionId): PositionResource
    {
        $position = $this->service->update(
            $this->dto::create($request->all(), $positionId)
        );
        return new PositionResource($position);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/positions/{positionId}",
     *     summary="Delete a position by ID",
     *     description="Delete a position record by its ID",
     *     tags={"Position"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="positionId",
     *         in="path",
     *         description="ID of the position to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Position deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Successfully deleted")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Position not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Position not found")
     *         )
     *     )
     * )
     */
    public function destroy(int $positionId): JsonResponse
    {
        $this->service->delete($positionId);
        return response()->json([
            'message' => 'Successfully deleted'
        ]);
    }
}
