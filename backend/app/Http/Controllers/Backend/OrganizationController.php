<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\DTO\OrganizationDto;
use App\DTO\OrganizationFilterDto;
use App\Http\Requests\OrganizationRequest;
use App\Http\Resources\OrganizationResource;
use App\Services\Backend\OrganizationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OrganizationController extends BaseBackendController
{

    public function __construct(
        private readonly OrganizationService   $service,
        private readonly OrganizationDto       $dto,
        private readonly OrganizationFilterDto $filterDto
    )
    {
    }

    /**
     * @OA\Get(
     *     path="/api/v1/organizations",
     *     summary="List organizations",
     *     description="Retrieve a list of organizations",
     *     tags={"Organization"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="take",
     *         in="query",
     *         description="Number of records to take",
     *         required=false,
     *         @OA\Schema(type="integer", format="int32", minimum=0, maximum=500)
     *     ),
     *     @OA\Parameter(
     *         name="skip",
     *         in="query",
     *         description="Number of records to skip",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="from_created_at",
     *         in="query",
     *         description="Filter organizations created after this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="to_created_at",
     *         in="query",
     *         description="Filter organizations created before this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="order_by",
     *         in="query",
     *         description="Order by a specific field",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="direction",
     *         in="query",
     *         description="Sort direction (asc or desc)",
     *         required=false,
     *         @OA\Schema(type="string", enum={"asc", "desc"})
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *              @OA\Property(property="data", type="json", ref="#/components/schemas/OrganizationResourceCollection"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     * @OA\Schema(
     *      schema="OrganizationResourceCollection",
     *      type="array",
     *      @OA\Items(
     *          @OA\Property(property="id", type="integer"),
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="description", type="string"),
     *          @OA\Property(property="created_at", type="string", format="date-time"),
     *       )
     *   )
     * @OA\Schema(
     *      schema="OrganizationResource",
     *          @OA\Property(property="id", type="integer"),
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="description", type="string"),
     *          @OA\Property(property="created_at", type="string", format="date-time"),
     *   )
     *
     */
    public function index(OrganizationRequest $request): AnonymousResourceCollection
    {
        $organizations = $this->service->index(
            $this->filterDto::create($request->all())
        );
        return OrganizationResource::collection($organizations);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/organizations",
     *     summary="Create a new organization",
     *     description="Create a new organization record",
     *     tags={"Organization"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Organization data",
     *         @OA\JsonContent(
     *             required={"name"},
     *             @OA\Property(property="name", type="string", example="Company Inc."),
     *             @OA\Property(property="description", type="string", example="Description of the organization"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Organization created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/OrganizationResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function store(OrganizationRequest $request): OrganizationResource
    {
        $organization = $this->service->store(
            $this->dto::create($request->all())
        );

        return new OrganizationResource($organization);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/organizations/{organizationId}",
     *     summary="Get an organization by ID",
     *     description="Retrieve an organization record by its ID",
     *     tags={"Organization"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="organizationId",
     *         in="path",
     *         description="ID of the organization to retrieve",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Organization retrieved successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/OrganizationResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organization not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Organization not found")
     *         )
     *     )
     * )
     */
    public function show(int $organizationId): OrganizationResource
    {
        $organization = $this->service->show($organizationId);
        return new OrganizationResource($organization);
    }

    /**
     * @OA\Put(
     *     path="/api/v1/organizations/{organizationId}",
     *     summary="Update an existing organization",
     *     description="Update an existing organization record",
     *     tags={"Organization"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="organizationId",
     *         in="path",
     *         description="ID of the organization to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated organization data",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Company ABC"),
     *             @OA\Property(property="description", type="string", example="Description of the organization"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Organization updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/OrganizationResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organization not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Organization not found")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function update(OrganizationRequest $request, int $organizationId): OrganizationResource
    {
        $organization = $this->service->update(
            $this->dto::create($request->all(), $organizationId)
        );
        return new OrganizationResource($organization);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/organizations/{organizationId}",
     *     summary="Delete an organization by ID",
     *     description="Delete an organization record by its ID",
     *     tags={"Organization"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="organizationId",
     *         in="path",
     *         description="ID of the organization to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Organization deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Successfully deleted")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organization not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Organization not found")
     *         )
     *     )
     * )
     */
    public function destroy(int $organizationId): JsonResponse
    {
        $this->service->delete($organizationId);
        return response()->json([
            'message' => 'Successfully deleted'
        ]);
    }
}
