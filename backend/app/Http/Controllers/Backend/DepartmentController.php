<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\DTO\DepartmentDto;
use App\DTO\DepartmentFilterDto;
use App\Http\Requests\DepartmentRequest;
use App\Http\Resources\DepartmentResource;
use App\Services\Backend\DepartmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DepartmentController extends BaseBackendController
{
    public function __construct(
        private readonly DepartmentService   $service,
        private readonly DepartmentDto       $dto,
        private readonly DepartmentFilterDto $filterDto
    )
    {
    }

    /**
     * @OA\Get(
     *     path="/api/v1/departments",
     *     summary="List departments",
     *     description="Retrieve a list of departments",
     *     tags={"Department"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="take",
     *         in="query",
     *         description="Number of records to take",
     *         required=false,
     *         @OA\Schema(type="integer", format="int32", minimum=0, maximum=500)
     *     ),
     *     @OA\Parameter(
     *         name="skip",
     *         in="query",
     *         description="Number of records to skip",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="from_created_at",
     *         in="query",
     *         description="Filter departments created after this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="to_created_at",
     *         in="query",
     *         description="Filter departments created before this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="order_by",
     *         in="query",
     *         description="Order by a specific field",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="direction",
     *         in="query",
     *         description="Sort direction (asc or desc)",
     *         required=false,
     *         @OA\Schema(type="string", enum={"asc", "desc"})
     *     ),
     *     @OA\Parameter(
     *         name="organization_id",
     *         in="query",
     *         description="Filter departments by organization ID",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *                 @OA\Property(property="data", type="json", ref="#/components/schemas/DepartmentResourceCollection"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     * @OA\Schema(
     *      schema="DepartmentResourceCollection",
     *                  type="array",
     *              @OA\Items(
     *          @OA\Property(property="id", type="integer"),
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="description", type="string"),
     *          @OA\Property(property="organization_id", type="integer"),
     *          @OA\Property(property="created_at", type="string", format="date-time"),
     *              )
     *  )
     * @OA\Schema(
     *      schema="DepartmentResource",
     *          @OA\Property(property="id", type="integer"),
     *          @OA\Property(property="name", type="string"),
     *          @OA\Property(property="description", type="string"),
     *          @OA\Property(property="organization_id", type="integer"),
     *          @OA\Property(property="created_at", type="string", format="date-time"),
     *  )
     */
    public function index(DepartmentRequest $request): AnonymousResourceCollection
    {
        $departments = $this->service->index(
            $this->filterDto::create($request->all())
        );
        return DepartmentResource::collection($departments);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/departments",
     *     summary="Create a new department",
     *     description="Create a new department record",
     *     tags={"Department"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Department data",
     *         @OA\JsonContent(
     *             required={"name", "organization_id"},
     *             @OA\Property(property="name", type="string", example="Marketing"),
     *             @OA\Property(property="description", type="string", example="Description of the department"),
     *             @OA\Property(property="organization_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Department created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/DepartmentResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function store(DepartmentRequest $request): DepartmentResource
    {
        $department = $this->service->store(
            $this->dto::create($request->all())
        );

        return new DepartmentResource($department);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/departments/{departmentId}",
     *     summary="Get a department by ID",
     *     description="Retrieve a department record by its ID",
     *     tags={"Department"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="departmentId",
     *         in="path",
     *         description="ID of the department to retrieve",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Department retrieved successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/DepartmentResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Department not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Department not found")
     *         )
     *     )
     * )
     */
    public function show(int $departmentId): DepartmentResource
    {
        $department = $this->service->show($departmentId);
        return new DepartmentResource($department);
    }

    /**
     * @OA\Put(
     *     path="/api/v1/departments/{departmentId}",
     *     summary="Update an existing department",
     *     description="Update an existing department record",
     *     tags={"Department"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="departmentId",
     *         in="path",
     *         description="ID of the department to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated department data",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", example="Marketing"),
     *             @OA\Property(property="description", type="string", example="Description of the department"),
     *             @OA\Property(property="organization_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Department updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/DepartmentResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Department not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Department not found")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function update(DepartmentRequest $request, int $departmentId): DepartmentResource
    {
        $department = $this->service->update(
            $this->dto::create($request->all(), $departmentId)
        );
        return new DepartmentResource($department);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/departments/{departmentId}",
     *     summary="Delete a department by ID",
     *     description="Delete a department record by its ID",
     *     tags={"Department"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="departmentId",
     *         in="path",
     *         description="ID of the department to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Department deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Successfully deleted")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Department not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Department not found")
     *         )
     *     )
     * )
     */
    public function destroy(int $departmentId): JsonResponse
    {
        $this->service->delete($departmentId);
        return response()->json([
            'message' => 'Successfully deleted'
        ]);
    }
}
