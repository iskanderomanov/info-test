<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\DTO\EmployeeDto;
use App\DTO\EmployeeFilterDto;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Services\Backend\EmployeeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EmployeeController extends BaseBackendController
{
    public function __construct(
        private readonly EmployeeService   $service,
        private readonly EmployeeDto       $dto,
        private readonly EmployeeFilterDto $filterDto
    )
    {
    }

    /**
     * @OA\Get(
     *     path="/api/v1/employees",
     *     summary="List employees",
     *     description="Retrieve a list of employees",
     *     tags={"Employee"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="take",
     *         in="query",
     *         description="Number of records to take",
     *         required=false,
     *         @OA\Schema(type="integer", format="int32", minimum=0, maximum=500)
     *     ),
     *     @OA\Parameter(
     *         name="skip",
     *         in="query",
     *         description="Number of records to skip",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="from_created_at",
     *         in="query",
     *         description="Filter employees created after this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="to_created_at",
     *         in="query",
     *         description="Filter employees created before this date",
     *         required=false,
     *         @OA\Schema(type="string", format="date")
     *     ),
     *     @OA\Parameter(
     *         name="order_by",
     *         in="query",
     *         description="Order by a specific field",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="direction",
     *         in="query",
     *         description="Sort direction (asc or desc)",
     *         required=false,
     *         @OA\Schema(type="string", enum={"asc", "desc"})
     *     ),
     *     @OA\Parameter(
     *         name="position_id",
     *         in="query",
     *         description="Filter employees by position ID",
     *         required=false,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *                 @OA\Property(property="data", type="json", ref="#/components/schemas/EmployeeResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     )
     * )
     *
     * @OA\Schema(
     *       schema="EmployeeResource",
     *                   type="array",
     *               @OA\Items(
     *           @OA\Property(property="id", type="integer"),
     *           @OA\Property(property="first_name", type="string"),
     *           @OA\Property(property="middle_name", type="string"),
     *           @OA\Property(property="last_name", type="string"),
     *           @OA\Property(property="birth_date", type="string", format="date"),
     *           @OA\Property(property="position_id", type="integer"),
     *           @OA\Property(property="is_man", type="boolean"),
     *           @OA\Property(property="created_at", type="string", format="date-time"),
     *               )
     *   )
     */
    public function index(EmployeeRequest $request): AnonymousResourceCollection
    {
        $employees = $this->service->index(
            $this->filterDto::create($request->all())
        );
        return EmployeeResource::collection($employees);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/employees",
     *     summary="Create a new employee",
     *     description="Create a new employee record",
     *     tags={"Employee"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Employee data",
     *         @OA\JsonContent(
     *             required={"first_name", "middle_name", "last_name", "birth_date", "is_man", "position_id"},
     *             @OA\Property(property="first_name", type="string", example="John"),
     *             @OA\Property(property="middle_name", type="string", example="Doe"),
     *             @OA\Property(property="last_name", type="string", example="Smith"),
     *             @OA\Property(property="birth_date", type="string", format="date", example="1990-01-01"),
     *             @OA\Property(property="is_man", type="boolean", example="true"),
     *             @OA\Property(property="position_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Employee created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/EmployeeResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function store(EmployeeRequest $request): EmployeeResource
    {
        $employee = $this->service->store(
            $this->dto::create($request->all())
        );

        return new EmployeeResource($employee);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/employees/{employeeId}",
     *     summary="Get an employee by ID",
     *     description="Retrieve an employee record by its ID",
     *     tags={"Employee"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="ID of the employee to retrieve",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Employee retrieved successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/EmployeeResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Employee not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Employee not found")
     *         )
     *     )
     * )
     */
    public function show(int $employeeId): EmployeeResource
    {
        $employee = $this->service->show($employeeId);
        return new EmployeeResource($employee);
    }


    /**
     * @OA\Put(
     *     path="/api/v1/employees/{employeeId}",
     *     summary="Update an existing employee",
     *     description="Update an existing employee record",
     *     tags={"Employee"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="ID of the employee to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Updated employee data",
     *         @OA\JsonContent(
     *             @OA\Property(property="first_name", type="string", example="John"),
     *             @OA\Property(property="middle_name", type="string", example="Doe"),
     *             @OA\Property(property="last_name", type="string", example="Smith"),
     *             @OA\Property(property="birth_date", type="string", format="date", example="1990-01-01"),
     *             @OA\Property(property="is_man", type="boolean", example="true"),
     *             @OA\Property(property="position_id", type="integer", example="1"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Employee updated successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="data", type="json", ref="#/components/schemas/EmployeeResource"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Employee not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Employee not found")
     *         )
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Validation error",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="The given data was invalid."),
     *             @OA\Property(property="errors", type="object", ref="#/components/schemas/ValidationError"),
     *         )
     *     )
     * )
     */
    public function update(EmployeeRequest $request, int $employeeId): EmployeeResource
    {
        $employee = $this->service->update(
            $this->dto::create($request->all(), $employeeId)
        );
        return new EmployeeResource($employee);
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/employees/{employeeId}",
     *     summary="Delete an employee by ID",
     *     description="Delete an employee record by its ID",
     *     tags={"Employee"},
     *     security={{ "bearerAuth":{} }},
     *     @OA\Parameter(
     *         name="employeeId",
     *         in="path",
     *         description="ID of the employee to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Employee deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Successfully deleted")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Employee not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Employee not found")
     *         )
     *     )
     * )
     */
    public function destroy(int $employeeId): JsonResponse
    {
        $this->service->delete($employeeId);
        return response()->json([
            'message' => 'Successfully deleted'
        ]);
    }
}
