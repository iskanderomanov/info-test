<?php

declare(strict_types=1);

namespace App\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryContract
{
    public function get(array|string $columns = ['*']): ?Collection;

    public function find(int $id, $columns = ['*']): ?Model;

    public function findOrFail(int $id, $columns = ['*']): Model;

    public function updateOrCreate(array $attributes, array $values = []): Model;

    public function create(array $attributes): Model;

    public function update(Model $model, array $attributes): ?Model;
    public function updateWhereId(int $id, array $attributes): ?Model;
    public function delete(Model $model): bool|null;
    public function deleteWhereId(int $id): bool;

}
