<?php
declare(strict_types=1);

namespace App\Enums;

enum RouteNames: string
{
    case MainOrganizationRoute = 'organizations';
    case IndexOrganizationRoute = 'organizations.index';
    case StoreOrganizationRoute = 'organizations.store';
    case UpdateOrganizationRoute = 'organizations.update';
    case ShowOrganizationRoute = 'organizations.show';
    case DestroyOrganizationRoute = 'organizations.destroy';

    case MainDepartmentRoute = 'departments';
    case IndexDepartmentRoute = 'departments.index';
    case StoreDepartmentRoute = 'departments.store';
    case UpdateDepartmentRoute = 'departments.update';
    case ShowDepartmentRoute = 'departments.show';
    case DestroyDepartmentRoute = 'departments.destroy';

    case MainPositionRoute = 'positions';
    case IndexPositionRoute = 'positions.index';
    case StorePositionRoute = 'positions.store';
    case UpdatePositionRoute = 'positions.update';
    case ShowPositionRoute = 'positions.show';
    case DestroyPositionRoute = 'positions.destroy';

    case MainEmployeeRoute = 'employees';
    case IndexEmployeeRoute = 'employees.index';
    case StoreEmployeeRoute = 'employees.store';
    case UpdateEmployeeRoute = 'employees.update';
    case ShowEmployeeRoute = 'employees.show';
    case DestroyEmployeeRoute = 'employees.destroy';

    case LoginUserRoute = 'login';
    case LogoutUserRoute = 'logout';
    case GetUserRoute = 'get.user';

    public function toRouteUrl(): string
    {
        return match ($this) {

            self::MainOrganizationRoute, self::IndexOrganizationRoute, self::StoreOrganizationRoute => 'organizations',
            self::UpdateOrganizationRoute, self::ShowOrganizationRoute, self::DestroyOrganizationRoute => 'organizations/{id}',

            self::IndexDepartmentRoute, self::MainDepartmentRoute, self::StoreDepartmentRoute => 'departments',
            self::UpdateDepartmentRoute, self::ShowDepartmentRoute, self::DestroyDepartmentRoute => 'departments/{id}',

            self::IndexPositionRoute, self::MainPositionRoute, self::StorePositionRoute => 'positions',
            self::UpdatePositionRoute, self::ShowPositionRoute, self::DestroyPositionRoute => 'positions/{id}',

            self::MainEmployeeRoute, self::IndexEmployeeRoute, self::StoreEmployeeRoute => 'employees',
            self::UpdateEmployeeRoute, self::ShowEmployeeRoute, self::DestroyEmployeeRoute => 'employees/{id}',

            self::LoginUserRoute => 'login',
            self::LogoutUserRoute => 'logout',
            self::GetUserRoute => 'get-user',
        };
    }
}
