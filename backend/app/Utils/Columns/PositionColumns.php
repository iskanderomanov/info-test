<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class PositionColumns extends BaseColumns
{
    public const TABLE_NAME = 'positions';
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const DEPARTMENT_ID = 'department_id';
}
