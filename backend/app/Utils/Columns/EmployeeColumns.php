<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class EmployeeColumns extends BaseColumns
{
    public const TABLE_NAME = 'employees';
    public const ID = 'id';
    public const FIRST_NAME = 'first_name';
    public const MIDDLE_NAME = 'middle_name';
    public const LAST_NAME = 'last_name';
    public const BIRTH_DATE = 'birth_date';
    public const IS_MAN = 'is_man';

    public const POSITION_ID = 'position_id';
}
