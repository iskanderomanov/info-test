<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class BaseColumns
{
    public const ID = 'id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = 'updated_at';
}
