<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class OrganizationColumns extends BaseColumns
{
    public const TABLE_NAME = 'organizations';

    public const NAME = 'name';
    public const DESCRIPTION = 'description';
}
