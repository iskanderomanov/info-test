<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class DepartmentColumns extends BaseColumns
{
    public const TABLE_NAME = 'departments';
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const ORGANIZATION_ID = 'organization_id';
}
