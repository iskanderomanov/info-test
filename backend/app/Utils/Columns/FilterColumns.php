<?php
declare(strict_types=1);

namespace App\Utils\Columns;

class FilterColumns
{
    public const TAKE = 'take';
    public const SKIP = 'skip';
    public const TO_CREATED_AT = 'to_created_at';
    public const FROM_CREATED_AT = 'from_created_at';
    public const UPDATED_AT = 'created_at';
    public const ORDER_BY = 'order_by';
    public const DIRECTION = 'direction';
}
