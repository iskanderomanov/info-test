<?php

namespace App\Models;

use App\Utils\Columns\EmployeeColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        EmployeeColumns::FIRST_NAME,
        EmployeeColumns::LAST_NAME,
        EmployeeColumns::MIDDLE_NAME,
        EmployeeColumns::BIRTH_DATE,
        EmployeeColumns::IS_MAN,
        EmployeeColumns::POSITION_ID
    ];

    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }
}
