<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Utils\Columns\UserColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        UserColumns::NAME,
        UserColumns::EMAIL,
        UserColumns::PASSWORD,
    ];

    protected $hidden = [
        UserColumns::PASSWORD,
        UserColumns::REMEMBER_TOKEN,
    ];

    protected $casts = [
        UserColumns::EMAIL_VERIFIED_AT => 'datetime',
        UserColumns::PASSWORD => 'hashed',
    ];
}
