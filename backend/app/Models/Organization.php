<?php

namespace App\Models;

use App\Utils\Columns\OrganizationColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Organization extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        OrganizationColumns::NAME,
        OrganizationColumns::DESCRIPTION,
    ];

    public function departments(): HasMany
    {
        return $this->hasMany(Department::class);
    }
}
