<?php

namespace App\Models;

use App\Utils\Columns\DepartmentColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Department extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        DepartmentColumns::NAME,
        DepartmentColumns::DESCRIPTION,
        DepartmentColumns::ORGANIZATION_ID,
    ];

    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function positions(): HasMany
    {
        return $this->hasMany(Position::class);
    }
}
