<?php

namespace App\Models;

use App\Utils\Columns\PositionColumns;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Position extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        PositionColumns::NAME,
        PositionColumns::DESCRIPTION,
        PositionColumns::DEPARTMENT_ID,
    ];

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    public function employees(): HasMany
    {
        return $this->hasMany(Employee::class);
    }
}
