<?php
declare(strict_types=1);


namespace App\DTO;

class AuthDto extends BaseDto
{
    public int $id;
    public string $email;
    public string $password;
}
