<?php
declare(strict_types=1);


namespace App\DTO;

class EmployeeDto extends BaseDto
{
    public int $id;
    public string $first_name;
    public string $middle_name;
    public string $last_name;
    public string $birth_date;
    public int $is_man;
    public int $position_id;
}
