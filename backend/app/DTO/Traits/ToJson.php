<?php
declare(strict_types=1);


namespace App\DTO\Traits;

trait ToJson
{
    public function toJson(): string
    {
        return json_encode(get_object_vars($this));
    }

}
