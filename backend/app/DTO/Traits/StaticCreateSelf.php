<?php
declare(strict_types=1);


namespace App\DTO\Traits;

trait StaticCreateSelf
{
    public static function create(array $values, int $id = null): static
    {
        $dto = new static();
        if ($id){
            $dto->id = $id;
        }
        foreach ($values as $key => $value) {
            if (property_exists($dto, $key)) {
                $dto->$key = is_numeric($value) ? (int)$value : $value;
            }
        }

        return $dto;
    }
}
