<?php
declare(strict_types=1);


namespace App\DTO;


use App\DTO\Traits\StaticCreateSelf;
use App\DTO\Traits\ToJson;

abstract class BaseDto
{
    use StaticCreateSelf, ToJson;

    public function get(string $variable): mixed
    {
        if (property_exists($this, $variable)) {
            return $this->$variable;
        } else {
            return null;
        }
    }

    public function set(string $variable, mixed $value): mixed
    {
        if (property_exists($this, $variable)) {
            return $this->$variable = $value;
        } else {
            return false;
        }
    }

    public function __invoke(bool $withId = false): array
    {
        $properties = get_object_vars($this);
        if (!$withId && isset($properties['id'])) {
            unset($properties['id']);
        }
        return $properties;
    }
}
