<?php
declare(strict_types=1);


namespace App\DTO;

class PositionFilterDto extends BaseDto
{
    public int $id;

    public int $department_id;
    public string $from_created_at;
    public string $to_created_at;

    public string $order_by;
    public string $direction;
    public int $skip;
    public int $take;
}
