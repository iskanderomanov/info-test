<?php
declare(strict_types=1);


namespace App\DTO;

class DepartmentDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $description;
    public int $organization_id;
}
