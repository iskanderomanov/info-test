<?php
declare(strict_types=1);


namespace App\DTO;

class PositionDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $description;
    public int $department_id;
}
