<?php
declare(strict_types=1);


namespace App\DTO;

class OrganizationDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $description;
}
