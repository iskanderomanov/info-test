<?php
declare(strict_types=1);


namespace App\DTO;

class EmployeeFilterDto extends BaseDto
{
    public int $id;

    public int $position_id;
    public bool $is_man;
    public string $from_created_at;
    public string $to_created_at;

    public string $order_by;
    public string $direction;
    public int $skip;
    public int $take;
}
