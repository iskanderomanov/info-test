<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\Employee;
use App\Utils\Columns\EmployeeColumns;
use Illuminate\Database\Eloquent\Collection;

class EmployeeRepository extends BaseRepository
{
    public function __construct(Employee $model)
    {
        parent::__construct($model);
    }

    public function filter(array $filters): Collection
    {
        $query = $this->newQuery();
        $this->applyCreatedAtFilter($query, $filters);
        $this->applyOrderBy($query, $filters, [
            EmployeeColumns::ID,
            EmployeeColumns::FIRST_NAME,
            EmployeeColumns::MIDDLE_NAME,
            EmployeeColumns::LAST_NAME,
            EmployeeColumns::BIRTH_DATE,
            EmployeeColumns::IS_MAN,
            EmployeeColumns::POSITION_ID,
            EmployeeColumns::CREATED_AT,
        ]);

        $this->applyPagination($query, $filters);
        return $query->get();
    }

}
