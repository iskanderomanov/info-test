<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\Department;
use App\Models\Position;
use App\Utils\Columns\DepartmentColumns;
use App\Utils\Columns\PositionColumns;
use Illuminate\Database\Eloquent\Collection;

class PositionRepository extends BaseRepository
{
    public function __construct(Position $model)
    {
        parent::__construct($model);
    }

    public function filter(array $filters): Collection
    {
        $query = $this->newQuery();
        $this->applyCreatedAtFilter($query, $filters);
        $this->applyOrderBy($query, $filters, [
            PositionColumns::NAME,
            PositionColumns::DESCRIPTION,
            PositionColumns::ID,
            PositionColumns::CREATED_AT,
            PositionColumns::DEPARTMENT_ID,
        ]);

        $this->applyPagination($query, $filters);
        return $query->get();
    }

}
