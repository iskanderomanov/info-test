<?php

namespace App\Repositories;

use App\Contracts\BaseRepositoryContract;

use App\Repositories\Traits\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements BaseRepositoryContract
{
    use Filterable;

    protected Model $model;

    public function __construct(Model|Builder $model)
    {
        $this->model = $model;
    }

    public function newInstance(array $attributes = [], bool $exists = false): Model
    {
        return $this->getModel()->newInstance($attributes, $exists);
    }

    public function getModel(): Model
    {
        return $this->model instanceof Model ? clone $this->model : $this->model->getModel();
    }

    public function newQuery(): Builder
    {
        return $this->model instanceof Model ? $this->model->newQuery() : clone $this->model;
    }

    public function filter(array $filters): Collection
    {
        $query = $this->newQuery();
        $this->applyCreatedAtFilter($query, $filters);
        $this->applyPagination($query, $filters);

        return $query->get();
    }

    public function get(array|string $columns = ['*']): ?Collection
    {
        return $this->newQuery()->get($columns);
    }

    public function find($id, $columns = ['*']): ?Model
    {
        return $this->newQuery()->find($id, $columns);
    }

    public function findOrFail($id, $columns = ['*']): Model
    {
        return $this->newQuery()->findOrFail($id, $columns);
    }

    public function updateOrCreate(array $attributes, array $values = []): Model
    {
        return $this->newQuery()->updateOrCreate($attributes, $values);
    }

    public function create(array $attributes): Model
    {
        return $this->newQuery()->create($attributes);
    }

    public function update(Model $model, array $attributes): ?Model
    {
        return tap($model)->update($attributes);
    }

    public function updateWhereId(int $id, array $attributes): ?Model
    {
        return tap($this->newQuery()->where('id', $id))->update($attributes)->firstOrFail();
    }

    public function delete(Model $model): bool|null
    {
        return $model->delete();
    }

    public function deleteWhereId(int $id): bool
    {
        return $this->newQuery()->where('id', $id)->delete();
    }

}
