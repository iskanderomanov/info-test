<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\User;
use Laravel\Sanctum\NewAccessToken;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function createAccessToken(User $user): NewAccessToken
    {
        return $user->createToken(
            'access_token',
            ['access_token'],
            now()->addMinutes(config('sanctum.expiration'))
        );
    }

}
