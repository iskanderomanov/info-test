<?php
declare(strict_types=1);


namespace App\Repositories\Traits;

use App\Utils\Columns\BaseColumns;
use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    protected function applyOrderBy(Builder $query, array $filters, array $allowedColumns = ['id', 'created_at']): void
    {
        $orderBy = $filters['order_by'] ?? 'id';
        $direction = isset($filters['direction']) && in_array(strtolower($filters['direction']), ['asc', 'desc'])
            ? $filters['direction']
            : 'asc';

        if (!in_array($orderBy, $allowedColumns)) {
            return;
        }

        $query->orderBy($orderBy, $direction);
    }

    protected function applyCreatedAtFilter(Builder $query, array $filters): void
    {
        if (isset($filters['from_created_at'])) {
            $query->whereDate(BaseColumns::CREATED_AT, '>=', $filters['from_created_at']);
        }

        if (isset($filters['to_created_at'])) {
            $query->whereDate(BaseColumns::CREATED_AT, '<=', $filters['to_created_at']);
        }
    }

    protected function applyPagination(Builder $query, array $filters): void
    {
        $skip = $filters['skip'] ?? 0;
        $take = $filters['take'] ?? 15;

        $query->skip($skip)->take($take);
    }
}
