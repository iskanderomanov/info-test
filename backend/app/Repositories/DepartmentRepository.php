<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\Department;
use App\Utils\Columns\DepartmentColumns;
use Illuminate\Database\Eloquent\Collection;

class DepartmentRepository extends BaseRepository
{
    public function __construct(Department $model)
    {
        parent::__construct($model);
    }

    public function filter(array $filters): Collection
    {
        $query = $this->newQuery();
        $this->applyCreatedAtFilter($query, $filters);
        $this->applyOrderBy($query, $filters, [
            DepartmentColumns::NAME,
            DepartmentColumns::DESCRIPTION,
            DepartmentColumns::ID,
            DepartmentColumns::CREATED_AT,
            DepartmentColumns::ORGANIZATION_ID,
        ]);

        $this->applyPagination($query, $filters);
        return $query->get();
    }

}
