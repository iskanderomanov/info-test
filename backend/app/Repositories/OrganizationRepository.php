<?php
declare(strict_types=1);

namespace App\Repositories;


use App\Models\Organization;
use App\Utils\Columns\OrganizationColumns;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class OrganizationRepository extends BaseRepository
{
    public function __construct(Organization $model)
    {
        parent::__construct($model);
    }

    public function filter(array $filters): Collection
    {
        $query = $this->newQuery();
        $this->applyCreatedAtFilter($query, $filters);
        $this->applyOrderBy($query, $filters, [
            OrganizationColumns::NAME,
            OrganizationColumns::DESCRIPTION,
            OrganizationColumns::ID,
            OrganizationColumns::CREATED_AT,
        ]);

        $this->applyPagination($query, $filters);
        return $query->get();
    }

}
