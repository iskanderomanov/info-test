<?php
declare(strict_types=1);


namespace App\Services\Backend;

use App\DTO\BaseDto;
use App\Repositories\EmployeeRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class EmployeeService extends BaseBackendService
{
    public function __construct(
        private readonly EmployeeRepository $repository,
    )
    {
    }

    public function index(BaseDto $dto): ?Collection
    {
        return $this->repository->filter($dto());
    }

    public function store(BaseDto $dto): ?Model
    {
        return $this->repository->create($dto());
    }

    public function show(int $id): ?Model
    {
        return $this->repository->findOrFail($id);
    }

    public function update(BaseDto $dto): ?Model
    {
        return $this->repository->updateWhereId($dto->get('id'), $dto());
    }

    public function delete(int $id): bool
    {
        return $this->repository->deleteWhereId($id);
    }
}
