<?php
declare(strict_types=1);

namespace App\Services\Backend;

use App\DTO\BaseDto;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthService extends BaseBackendService
{
    public function __construct(
        private readonly UserRepository $userRepository
    )
    {
    }

    /**
     * @throws ValidationException
     */
    public function login(BaseDto $dto): array
    {
        if (!Auth::attempt($dto())) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        $user = Auth::user();
        $user->tokens()->delete();

        return $this->generateAuthResponse($user);
    }

    public function logout(): void
    {
        Auth::user()->tokens()->delete();
    }

    private function generateAuthResponse($user): array
    {
        $accessToken = $this->userRepository->createAccessToken($user);
        $expiresAt = $accessToken->accessToken->expires_at->format('Y-m-d H:i:s');

        return [
            'user' => $user,
            'access_token' => $accessToken->plainTextToken,
            'expiresAt' => $expiresAt
        ];
    }
}
