<?php

namespace Database\Seeders;

use App\Repositories\UserRepository;
use App\Utils\Columns\UserColumns;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run(UserRepository $userRepository): void
    {
        $userRepository->create([
            UserColumns::NAME => 'develop',
            UserColumns::EMAIL => 'dev@dev.dev',
            UserColumns::PASSWORD => Hash::make('develop123'),
            UserColumns::CREATED_AT => Carbon::now(),
            UserColumns::UPDATED_AT => Carbon::now(),
        ]);
    }
}
