<?php

namespace Database\Seeders;

use App\Repositories\EmployeeRepository;
use App\Repositories\PositionRepository;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    public function run(
        EmployeeRepository $employeeRepository,
        PositionRepository $positionRepository
    ): void
    {
        $positions = $positionRepository->get();

        $maleFirstNames = ['Айбек', 'Акмат', 'Бакыт', 'Болот', 'Жаныбек', 'Эрмек'];
        $maleMiddleNames = ['Абдылдаевич', 'Абдыкадырович', 'Абдылдаевич', 'Абдыкадырович', 'Абдылдаевич', 'Абдылдаевич'];
        $maleLastNames = ['Абдылдаев', 'Абдыкадыров', 'Абдылдаев', 'Абдыкадыров', 'Абдылдаев', 'Абдылдаев'];
        $femaleFirstNames = ['Айгерим', 'Айгуль', 'Айзада', 'Алтынай', 'Гулнур', 'Зарина'];
        $femaleMiddleNames = ['Абдылдаевна', 'Абдыкадыровна', 'Абдылдаевна', 'Абдыкадыровна', 'Абдылдаевна', 'Абдылдаевна'];
        $femaleLastNames = ['Абдылдаева', 'Абдыкадырова', 'Абдылдаева', 'Абдыкадырова', 'Абдылдаева', 'Абдылдаева'];

        foreach ($positions as $position) {
            for ($i = 1; $i <= rand(1, 3); $i++) {
                $employee = $employeeRepository->getModel();
                if ($position->department->organization->id % 2 == 0) {
                    $employee->first_name = $maleFirstNames[array_rand($maleFirstNames)];
                    $employee->middle_name = $maleMiddleNames[array_rand($maleMiddleNames)];
                    $employee->last_name = $maleLastNames[array_rand($maleLastNames)];
                    $employee->is_man = true;
                } else {
                    $employee->first_name = $femaleFirstNames[array_rand($femaleFirstNames)];
                    $employee->middle_name = $femaleMiddleNames[array_rand($femaleMiddleNames)];
                    $employee->last_name = $femaleLastNames[array_rand($femaleLastNames)];
                    $employee->is_man = false;
                }
                $employee->birth_date = now()->subYears(rand(20, 60))->subDays(rand(0, 365));
                $employee->position_id = $position->id;
                $employee->save();
            }
        }
    }
}
