<?php

namespace Database\Seeders;

use App\Repositories\DepartmentRepository;
use App\Repositories\PositionRepository;
use App\Utils\Columns\PositionColumns;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    public function run(PositionRepository $positionRepository, DepartmentRepository $departmentRepository): void
    {
        $departments = $departmentRepository->get();
        $positions = [
            'Менеджер', 'Инженер', 'Программист', 'Финансовый аналитик', 'Маркетолог', 'Специалист по персоналу', 'Продавец'
        ];
        foreach ($departments as $department) {
            foreach ($positions as $positionName) {
                $positionRepository->create([
                    PositionColumns::NAME => $positionName,
                    PositionColumns::DESCRIPTION => "Должность {$positionName} в отделе '{$department->name}'",
                    PositionColumns::DEPARTMENT_ID => $department->id,
                ]);
            }
        }
    }
}
