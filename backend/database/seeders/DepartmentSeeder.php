<?php

namespace Database\Seeders;

use App\Repositories\DepartmentRepository;
use App\Repositories\OrganizationRepository;
use App\Utils\Columns\DepartmentColumns;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{

    public function run(
        DepartmentRepository   $departmentRepository,
        OrganizationRepository $organizationRepository
    ): void
    {
        $organizations = $organizationRepository->get();
        $departments = [
            'Разработка', 'Производство', 'Маркетинг', 'Финансы', 'Кадры', 'Продажи'
        ];

        foreach ($organizations as $organization) {
            foreach ($departments as $departmentName) {
                $departmentRepository->create([
                    DepartmentColumns::NAME => $departmentName,
                    DepartmentColumns::DESCRIPTION => "Отдел {$departmentName} в организации '{$organization->name}'",
                    DepartmentColumns::ORGANIZATION_ID => $organization->id,
                ]);
            }
        }
    }
}
