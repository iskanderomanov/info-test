<?php

namespace Database\Seeders;

use App\Repositories\OrganizationRepository;
use App\Utils\Columns\OrganizationColumns;
use Illuminate\Database\Seeder;

class OrganizationSeeder extends Seeder
{

    public function run(OrganizationRepository $organizationRepository): void
    {
        $organizationRepository->create([
            OrganizationColumns::NAME => 'Организация "Прогресс"',
            OrganizationColumns::DESCRIPTION => 'Главная организация',
        ]);

        $organizationRepository->create([
            OrganizationColumns::NAME => 'Технологическая Организация "Смарт"',
            OrganizationColumns::DESCRIPTION => 'Организация специализирующаяся на разработке и внедрении новых технологий',
        ]);

    }
}
