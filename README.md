# Инструкция по запуску проекта

## Шаг 1: Клонирование репозитория

Начните с клонирования репозитория на свою локальную машину. Это можно сделать с помощью команды:

```bash
git clone https://gitlab.com/iskanderomanov/info-test.git
```

## Шаг 2: Установка Docker и Docker Compose

Проект использует Docker и Docker Compose, поэтому вам нужно убедиться, что у вас установлены Docker и Docker Compose. Если у вас их еще нет, вы можете установить их, следуя официальной документации Docker.

## Шаг 3: Перейдите в каталог вашего проекта

```bash
cd info-test
```

## Шаг 4: Настройка окружения:

скопируйте содержимое файла `.env.example` в `.env`

```bash
cp backend/.env.example backend/.env
```

## Шаг 5: Сборка и запуск Docker контейнеров:

выполните команду сборки:

```bash
docker-compose build
```

Запустите контейнеры Docker с помощью команды:

```bash
docker-compose up -d
```

## Шаг 6: Установка зависимостей PHP

Выполните установку зависимостей Composer c помощью make команды:

```bash
make composer-i
```

```bash
make artisan a="key:generate"
```

## Шаг 7: Запуск миграций и сидеров:

выполните миграции и сидеры c помощью make команды:
``` bash
make artisan a="migrate --seed"
```
## Шаг 8: Создайте документацию Swagger

Выполните следующую команду, чтобы создать файлы документации Swagger:

```bash
make artisan a="l5-swagger:generate"
```

## Шаг 9: Доступ к  интерфейсу Swagger

Вы можете получить доступ к интерактивному интерфейсу пользовательского интерфейса Swagger, по URL-адресу: http://localhost:8000/api/documentation